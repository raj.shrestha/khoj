import Immutable from 'immutable'
import {EventEmitter}  from 'events'

import AppDispatcher from '../dispatcher/app.dispatcher'
import AppConstants from '../constants/app.constants'
import FrameUtil from '../utils/frame.util'

var CHANGE_EVENT = 'change';

let appState = Immutable.fromJS({
  activeFrameKey: null,
  fullScreen: false,
  frames: [],
  closedFrames: [],
  sites: [],
  navbar: {
    urlbar: {
      location: '',
      active: false,
      urlPreview: '',
      suggestions: {
        activeIndex: 0,
        searchResults: [],
        suggestionList: null
      },
      focused: true,
      autoselected: true
    }
  },
  tabs: {
    activeDraggedTab: null
  }
})

const updateUrl = (location) => {
  appState = appState.mergeIn(['frames', FrameUtil.findIndexOfFrameKey(appState.get('frames'), appState.get('activeFrameKey'))], {
    src: location,
    location: location,
  })
}

const updateUrlBarInput = (location) => {
  appState = appState.setIn(['navbar', 'urlbar', 'location'], location)
}

const setLocation = (location, key) => {
  key = key || appState.get('activeFrameKey')
  appState = appState.mergeIn(['frames', FrameUtil.findIndexOfFrameKey(appState.get('frames'), key)], {
    location: location
  })
  // Update the displayed location in the urlbar
  if (key === appState.get('activeFrameKey')) {
    if(location == 'about:blank')
      location=''
    updateUrlBarInput(location)
  }
}

class AppStore extends EventEmitter {
  getAppState () {
    return appState
  }

  getFrameCount () {
    return appState.get('frames').size
  }

  emitChange () {
    // this.emit(CHANGE_EVENT)
    setTimeout(() => this.emit(CHANGE_EVENT), 0)
  }

  addChangeListener (callback) {
    this.on(CHANGE_EVENT, callback)
  }

  removeChangeListener (callback) {
    this.removeListener(CHANGE_EVENT, callback)
  }
}

const appStore = new AppStore()
let currentKey = 0
const incrementNextKey = () => ++currentKey

// Register callback to handle all updates
AppDispatcher.register((action) => {
  switch (action.actionType) {
    case AppConstants.SET_URL:
      updateUrl(action.location)
      appStore.emitChange()
      break

    case AppConstants.SET_URLBAR_INPUT:
      updateUrlBarInput(action.location)
      appStore.emitChange()
      break

    case AppConstants.NEW_FRAME:
      let nextKey = incrementNextKey()
      let activeFrameKey = appState.get('activeFrameKey')
      appState = appState.merge(FrameUtil.addFrame(appState.get('frames'), action.frameProps,
        nextKey, nextKey))
      if(action.openInBackground) {
        appState = appState.merge({ activeFrameKey: activeFrameKey })
      }
      appStore.emitChange()
      break

    case AppConstants.SET_TITLE:
        appState = appState.mergeIn(['frames', FrameUtil.getFramePropsIndex(appState.get('frames'), action.frameProps)], {
          title: action.title
        })
        appStore.emitChange()
        break

    case AppConstants.WEBVIEW_LOADING_START:
      appState = appState.mergeIn(['frames', FrameUtil.getFramePropsIndex(appState.get('frames'), action.frameProps)], {
        loading: true
      })
      appStore.emitChange()
      break

    case AppConstants.WEBVIEW_LOADING_END:
      appState = appState.mergeIn(['frames', FrameUtil.getFramePropsIndex(appState.get('frames'), action.frameProps)], {
        loading: false
      })
      appStore.emitChange()
      break

    case AppConstants.SET_ACTIVE_FRAME:
      appState = appState.merge({
        activeFrameKey: action.frameProps.get('key')
      })
      appStore.emitChange()
      break

    case AppConstants.CLOSE_FRAME:
      let frameProps = action.frameProps || FrameUtil.getActiveFrame(appState)
      const index = FrameUtil.getFramePropsIndex(appState.get('frames'), frameProps)
      appState = appState.merge(FrameUtil.removeFrame(appState.get('frames'), appState.get('closedFrames'),  frameProps.set('closedAtIndex', index),
        FrameUtil.getActiveFrame(appState)))
      appStore.emitChange()
      break

    case AppConstants.UNDO_CLOSED_FRAME:
      appState = appState.merge(FrameUtil.undoCloseFrame(appState, appState.get('closedFrames')))
      appStore.emitChange()
      break

    case AppConstants.SET_URL_BAR_ACTIVE:
      appState = appState.setIn(['navbar', 'urlbar', 'active'], action.isActive)
      break

    case AppConstants.SET_URLBAR_FOCUSED:
      appState = appState.setIn(['navbar', 'urlbar', 'focused'], action.focused)
      appStore.emitChange()
      break

    case AppConstants.SET_LOCATION:
      setLocation(action.location, action.key)
      appStore.emitChange()
      break

    case AppConstants.UPDATE_BACKWARD_FORWARD:
      appState = appState.mergeIn(['frames', FrameUtil.getFramePropsIndex(appState.get('frames'), action.frameProps)], {
        canGoBack: action.canGoBack,
        canGoForward: action.canGoForward
      })
      appStore.emitChange()
      break

    case AppConstants.TAB_DRAG_START:
      appState = appState.mergeIn(['frames', FrameUtil.getFramePropsIndex(appState.get('frames'), action.frameProps)], {
        tabIsDragging: true
      })
      appState = appState.setIn(['tabs', 'activeDraggedTab'], action.frameProps)
      appStore.emitChange()
      break

    case AppConstants.TAB_DRAG_STOP:
      appState = appState.mergeIn(['frames', FrameUtil.getFramePropsIndex(appState.get('frames'), action.frameProps)], {
        tabIsDragging: false
      })
      appState = appState.setIn(['tabs', 'activeDraggedTab'], null)
      appStore.emitChange()
      break

    case AppConstants.TAB_DRAGGING_ON:
      appState = appState.mergeIn(['frames', FrameUtil.getFramePropsIndex(appState.get('frames'), action.frameProps)], {
        tabIsDraggingOn: true,
        tabIsDraggingOverLeftHalf: false,
        tabIsDraggingOverRightHalf: false
      })
      appStore.emitChange()
      break

    case AppConstants.TAB_DRAGGING_OVER_LEFT:
      appState = appState.mergeIn(['frames', FrameUtil.getFramePropsIndex(appState.get('frames'), action.frameProps)], {
        tabIsDraggingOn: false,
        tabIsDraggingOverLeftHalf: true,
        tabIsDraggingOverRightHalf: false
      })
      appStore.emitChange()
      break

    case AppConstants.TAB_DRAGGING_OVER_RIGHT:
      appState = appState.mergeIn(['frames', FrameUtil.getFramePropsIndex(appState.get('frames'), action.frameProps)], {
        tabIsDraggingOn: false,
        tabIsDraggingOverLeftHalf: false,
        tabIsDraggingOverRightHalf: true
      })
      appStore.emitChange()
      break

    case AppConstants.TAB_DRAG_EXIT:
      appState = appState.mergeIn(['frames', FrameUtil.getFramePropsIndex(appState.get('frames'), action.frameProps)], {
        tabIsDraggingOn: false,
        tabIsDraggingOverLeftHalf: false,
        tabIsDraggingOverRightHalf: false
      })
      appStore.emitChange()
      break

    case AppConstants.TAB_DRAG_EXIT_RIGHT:
      appState = appState.mergeIn(['frames', FrameUtil.getFramePropsIndex(appState.get('frames'), action.frameProps)], {
        tabIsDraggingOverRightHalf: false
      })
      appStore.emitChange()
      break

    case AppConstants.TAB_MOVE:
      let sourceFramePropsIndex = FrameUtil.getFramePropsIndex(appState.get('frames'), action.sourceFrameProps)
      let newIndex = FrameUtil.getFramePropsIndex(appState.get('frames'), action.destinationFrameProps) + (action.prepend ? 0 : 1)
      let frames = appState.get('frames').splice(sourceFramePropsIndex, 1)
      if (newIndex > sourceFramePropsIndex) {
        newIndex--
      }
      frames = frames.splice(newIndex, 0, action.sourceFrameProps)
      appState = appState.set('frames', frames)
      appStore.emitChange()
      break

    case AppConstants.SET_URL_BAR_SUGGESTIONS:
      appState = appState.setIn(['navbar', 'urlbar', 'suggestions', 'selectedIndex'], action.selectedIndex)
      appState = appState.setIn(['navbar', 'urlbar', 'suggestions', 'suggestionList'], action.suggestionList)
      appStore.emitChange()
      break

    case AppConstants.SET_URL_BAR_PREVIEW:
      appState = appState.setIn(['navbar', 'urlbar', 'urlPreview'], action.selectedIndex)
      appStore.emitChange()
      break

    case AppConstants.SET_URL_BAR_SUGGESTION_SEARCH_RESULTS:
      appState = appState.setIn(['navbar', 'urlbar', 'suggestions', 'searchResults'], action.searchResults)
      appStore.emitChange()
      break

    case AppConstants.TOGGLE_FULL_HTML_SCREEN:
      appState = appState.set('fullScreen', action.value)
      appStore.emitChange()
      break

    case AppConstants.SET_FRAME_FAVICONS:
      appState = appState.mergeIn(['frames', FrameUtil.getFramePropsIndex(appState.get('frames'), action.frameProps)], {
        favicons: action.favicons
      })
      appStore.emitChange()
      break

    case AppConstants.OPEN_FINDER_BAR:
      appState = appState.mergeIn(['frames', FrameUtil.getFramePropsIndex(appState.get('frames'), action.frameProps)], {
        finder: action.open
      })
      appStore.emitChange()
      break

    default:
  }
})

export default appStore

