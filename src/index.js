'use strict';

require('../sass/global.scss')
require('../sass/window.scss')
require('../sass/tab.scss')
require('../sass/frame.scss')
require('../sass/navigationbar.scss')
require('../node_modules/font-awesome/css/font-awesome.css')

import React from 'react'
import ReactDOM from 'react-dom'
import Window from './components/window.component'

ReactDOM.render(
  <Window />,
  document.getElementById('content')
)
