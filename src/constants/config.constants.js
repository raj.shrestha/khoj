var vaultHost = process.env.VAULT_HOST || 'http://localhost:3000'

export default {
  defaultUrl: 'about:blank',
  maxClosedFrames: 100,
  urlBarSuggestions: {
    maxTopSites: 5,
    maxSearch: 3,
    maxSites: 2,
    maxOpenedFrames: 2
  },
}