/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

import Immutable from 'immutable'
import Config from '../constants/config.constants'

function findIndexOfFrameKey (frames, key) {
  return frames.findIndex(frame => frame.get('key') === key)
}

function findIndexForFrameKey (frames, key) {
  return frames.findIndex(frame => frame.get('key') === key)
}

function getActiveFrameIndex(appState) {
  return findIndexForFrameKey(appState.get('frames'), appState.get('activeFrameKey'))
}

function getActiveFrame(appState) {
  const activeFrameIndex = getActiveFrameIndex(appState)
  return appState.get('frames').get(activeFrameIndex)
}

function getFramePropsIndex (frames, frameProps) {
  return frames.findIndex(found => found.get('key') === frameProps.get('key'))
}

function isFrameActive(appState, frame) {
  return getActiveFrame(appState) === frame
}

/**
 * Adds a frame with its frameProps and newKey and sets the activeFrameKey
 */
function addFrame (frames, frameProps, newKey, activeFrameKey) {
  var url = frameProps.location || Config.defaultUrl
  let frame = Immutable.fromJS({
    src: url,
    location: url,
    key: newKey,
    title: 'New Tab',
    parentFrameKey: frameProps.parentFrameKey,
    loading: false,
    canGoBack: false,
    canGoForward: false
  })

  // Find the closest index to the current frame's index which has
  // a different ancestor frame key.
  let insertionIndex = findIndexOfFrameKey(frames, frameProps.parentFrameKey)
  if (insertionIndex === -1) {
    insertionIndex = frames.size
  }
  while (insertionIndex < frames.size) {
    ++insertionIndex
    if (!isAncestorFrameKey(frames, frames.get(insertionIndex), frameProps.parentFrameKey)) {
      break
    }
  }

  return {
    frames: frames.splice(insertionIndex, 0, frame),
    activeFrameKey
  }
}

function removeFrame (frames, closedFrames, frameProps, activeFrameKey) {
  if (!frameProps.get('isPrivate')) {
    closedFrames = closedFrames.push(frameProps)
    if (closedFrames.size > Config.maxClosedFrames) {
      closedFrames = closedFrames.shift()
    }
  }
  const activeFrameIndex = findIndexForFrameKey(frames, activeFrameKey)
  const framePropsIndex = getFramePropsIndex(frames, frameProps)
  frames = frames.splice(framePropsIndex, 1)
  const newActiveFrameKey = frameProps.get('key') === activeFrameKey && frames.size > 0
      ? Math.max(
        frames.get(activeFrameIndex)
          // Go to the next frame if it exists.
          ? frames.get(activeFrameIndex).get('key')
          // Otherwise go to the frame right before the active tab.
          : frames.get(activeFrameIndex - 1).get('key'),
        0) : activeFrameKey
  return {
    activeFrameKey: newActiveFrameKey,
    closedFrames,
    frames
  }
}

function undoCloseFrame (appState, closedFrames) {
  if (closedFrames.size === 0) {
    return {}
  }
  var closedFrame = closedFrames.last()
  let insertIndex = closedFrame.get('closedAtIndex')
  return {
    closedFrames: closedFrames.pop(),
    frames: appState.get('frames').splice(insertIndex, 0, closedFrame.remove('closedAtIndex')),
    activeFrameKey: closedFrame.get('key')
  }
}

export default {
  findIndexOfFrameKey,
  getActiveFrame,
  getFramePropsIndex,
  isFrameActive,
  addFrame,
  removeFrame,
  undoCloseFrame
}
