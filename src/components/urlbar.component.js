import React from 'react'
import ReactDOM from 'react-dom'
import classNames from 'class-name'
import AppActions from '../actions/app.actions'
import ImmutableComponent from './immutable.component'
import UrlBarSuggestions from './urlbarSuggestions.component'

class UrlBar extends ImmutableComponent {
  constructor () {
    super()

    process.on('active-urlbar', () => {
      let urlInput = ReactDOM.findDOMNode(this.refs.urlInput)
      urlInput.focus()
    })
  }

  onKeyDown (e) {
    switch (e.keyCode) {
      case 13:
        e.preventDefault()
        let location = this.props.urlbar.get('location')
        if (location === null || location.length === 0) {
          this.restore()
          this.select()
        } else {
          AppActions.loadUrl(location)
          AppActions.setUrlBarActive(false)
          this.blur()
        }
        break

      default:
        AppActions.setUrlBarActive(true)
    }
  }

  onChange (e) {
    AppActions.setUrlBarInput(e.target.value)
  }

  focus () {
    let urlInput = ReactDOM.findDOMNode(this.refs.urlInput)
    urlInput.focus()
  }

  select () {
    let urlInput = ReactDOM.findDOMNode(this.refs.urlInput)
    urlInput.focus()
    urlInput.select()
  }

  blur () {
    let urlInput = ReactDOM.findDOMNode(this.refs.urlInput)
    urlInput.blur()
  }

  restore () {
    let location = this.props.activeFrameProps.get('location')
    AppActions.setUrlBarUserInput(location)
  }

  onBlur (e) {
    AppActions.setUrlBarFocused(false)
  }

  onFocus (e) {
    this.select()
    AppActions.setUrlBarFocused(true)
  }

  componentWillReceiveProps (newProps) {
    let location = newProps.activeFrameProps.get('location')
    let key = newProps.activeFrameProps.get('key')

    if (key !== this.props.activeFrameProps.get('key')) {
      AppActions.setLocation(location)
    }
  }

  render () {
    return (
      <div className='urlBarWrapper'>
        <form
          id='urlBar'
          ref='urlbar'
          className={classNames({
            focused: this.props.urlbar.get('focused')
          })}>
          <span className={classNames({
            fa: true,
            'fa-lock': false,
            'fa-unlock': false,
            'fa-globe': true
          })}></span>
          <input type='text'
            ref='urlInput'
            id='urlInput'
            placeholder='Enter url'
            onChange={this.onChange.bind(this)}
            onKeyDown={this.onKeyDown.bind(this)}
            value={this.props.urlbar.get('location')}
            onBlur={this.onBlur.bind(this)}
            onFocus={this.onFocus.bind(this)} />
        </form>
        <UrlBarSuggestions
          ref='urlBarSuggestions'
          suggestions={this.props.urlbar.get('suggestions')}
          sites={this.props.sites}
          frames={this.props.frames}
          searchDetail={this.props.searchDetail}
          searchSuggestions={this.props.searchSuggestions}
          activeFrameProps={this.props.activeFrameProps}
          urlLocation={this.props.urlbar.get('location')}
          urlPreview={this.props.urlbar.get('urlPreview')}
          previewActiveIndex={this.props.previewActiveIndex || 0} />
      </div>
    )
  }
}

export default UrlBar
