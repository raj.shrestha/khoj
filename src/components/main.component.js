import React from 'react'
import {ipcRenderer} from 'electron'
import classNames from 'class-name'
import AppActions from '../actions/app.actions'
import FrameUtil from '../utils/frame.util'
import Config from '../constants/config.constants'
import contextMenus from '../../app/contextMenu'

import ImmutableComponent from './immutable.component'
import Tabs from './tabs.component'
import NavigationBar from './navigationbar.component'
import Frame from './frame.component'

class Main extends ImmutableComponent{
  componentDidMount () {
    if (this.props.window.get('frames').isEmpty()) {
      AppActions.newFrame({
        location: Config.defaultUrl
      })
      //not working
      process.emit('active-urlbar')
    }

    ipcRenderer.on('context-menu-opened', (e, nodeProps) => {
      contextMenus.onContextMenu(nodeProps)
    })

    ipcRenderer.on('shortcut-new-frame', (e, src, openInBackground) => {
      AppActions.newFrame({
        location: src || Config.defaultUrl
      }, openInBackground)
      process.emit('active-urlbar')
    })

    ipcRenderer.on('shortcut-close-frame', () =>
       AppActions.closeFrame()
    )

    ipcRenderer.on('shortcut-undo-closed-frame', () =>
      AppActions.undoClosedFrame()
    )
  }

  render () {
    const sortByKeyAsc = (a, b) => a.get('key') > b.get('key')
      ? 1 : b.get('key') > a.get('key') ? -1 : 0

    const activeFrame = FrameUtil.getActiveFrame(this.props.window)

    return (
      <div id='window'>
        <div className={classNames({
          topContainer: true,
          fullScreen: this.props.window.get('fullScreen')
        })}>
          <Tabs
            tabs={this.props.window.get('tabs')}
            frames={this.props.window.get('frames')}
            activeFrame={activeFrame}
          />
          <NavigationBar
            navbar={this.props.window.get('navbar')}
            frames={this.props.window.get('frames')}
            activeFrame={activeFrame}
            sites={this.props.window.get('sites')}
            activeFrameKey={this.props.window.get('activeFrameKey')}
          />
        </div>
        <div className='frameContainer'>
          <div className='tabContainer'>
            {
              this.props.window.get('frames').sort(sortByKeyAsc).map(frame =>
                <Frame
                  ref={`frame${frame.get('key')}`}
                  key={frame.get('key')}
                  frame={frame}
                  isActive={FrameUtil.isFrameActive(this.props.window, frame)}
                />
              )
            }
          </div>
        </div>
      </div>
    )
  }
}

export default Main
