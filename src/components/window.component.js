import React from 'react'
import Immutable from 'immutable'
import AppStore from '../stores/app.stores'
import Main from './main.component'

class Window extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      immutableData: AppStore.getAppState()
    }
    AppStore.addChangeListener(this.onChange.bind(this))
  }

  componentWillUnmount () {
    AppStore.removeChangeListener(this.onChange.bind(this))
  }

  shouldComponentUpdate (nextProps, nextState) {
    return !Immutable.is(nextState.immutableData, this.state.immutableData)
  }

  onChange () {
    this.setState({
      immutableData: AppStore.getAppState()
    })
  }

  render() {
    return(
      <div id='windowContainer'>
        <Main window={this.state.immutableData}/>
      </div>
    )
  }
}

export default Window
