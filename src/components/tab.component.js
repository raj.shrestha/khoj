import React from 'react'
import ReactDOM from 'react-dom'
import classNames from 'class-name'
import AppActions from '../actions/app.actions'
import ImmutableComponent from './immutable.component'

class Draggable extends ImmutableComponent {
  constructor (props) {
    super(props)
  }

  render () {
    return <hr className={classNames({
      draggable: true,
      dragActive: this.props.active,
      draggableEnd: this.props.end
    })}/>
  }
}

class Tab extends ImmutableComponent {
  constructor (props) {
    super(props)
  }

  get titleValue () {
    return (this.props.frameProps.get('title') ||
    this.props.frameProps.get('location'))
  }

  get favicon () {
    let favicons = this.props.frameProps.get('favicons')
    return favicons ? favicons.first() : ''
  }

  setActiveFrame() {
    AppActions.setActiveFrame(this.props.frameProps)
  }

  closeFrame() {
    AppActions.closeFrame(this.props.frameProps)
  }

  onDragStart (e) {
    AppActions.tabDragStart(this.props.frameProps)
  }

  onDragEnd() {
    AppActions.tabDragStop(this.props.frameProps)
  }

  onDragOver(e) {
    e.preventDefault()
    // Otherise, only accept it if we have some frameProps
    if (!this.props.activeDraggedTab) {
      AppActions.tabDraggingOn(this.props.frameProps)
      return
    }

    let rect = ReactDOM.findDOMNode(this.refs.tab).getBoundingClientRect()
    if (e.clientX > rect.left && e.clientX < rect.left + rect.width / 2 &&
      !this.props.frameProps.get('tabIsDraggingOverLeftHalf')) {
      AppActions.tabDragDraggingOverLeftHalf(this.props.frameProps)
    } else if (e.clientX < rect.right && e.clientX >= rect.left + rect.width / 2 &&
      !this.props.frameProps.get('tabIsDraggingOverRightHalf')) {
       AppActions.tabDragDraggingOverRightHalf(this.props.frameProps)
    }
  }

  onDragLeave() {
    if (this.props.frameProps.get('tabIsDraggingOverLeftHalf') ||
     this.props.frameProps.get('tabIsDraggingOn') ||
     this.props.frameProps.get('tabIsDraggingOverLeftHalf')) {
     AppActions.tabDragExit(this.props.frameProps)
    } else if (this.props.frameProps.get('tabIsDraggingOverRightHalf')) {
     AppActions.tabDragExitRightHalf(this.props.frameProps)
    }
  }

  onDrop(e) {
    let sourceFrameProps = this.props.activeDraggedTab
    if (!sourceFrameProps) {
      return
    }

    if (this.props.frameProps.get('tabIsDraggingOverLeftHalf')) {
      AppActions.tabMove(sourceFrameProps, this.props.frameProps, true)
    } else {
      AppActions.tabMove(sourceFrameProps, this.props.frameProps, false)
    }
    AppActions.tabDragExit(this.props.frameProps)
  }

  render () {
    let isLoading = this.props.frameProps.get('loading')
    let backgroundImage = this.favicon
    const favIcon = isLoading ? {backgroundSize: 16} :{
      backgroundImage: `url(${backgroundImage})`,
      backgroundSize: 16,
      width: 16,
      height: 16
    }

    const loadingIcon = classNames({
      tabIcon: true,
      fa: isLoading,
      'fa-circle-o-notch': isLoading,
      'fa-spin': isLoading,
    })

    return (
      <div className='tabWrapper'>
        <Draggable active={this.props.frameProps.get('tabIsDraggingOverLeftHalf')}/>
        <div
            className={classNames({
                tab: true,
                active: this.props.isActive,
                draggingOn: this.props.frameProps.get('tabIsDraggingOn'),
                dragging: this.props.frameProps.get('tabIsDragging'),
                'dragging-over': this.props.frameProps.get('tabIsDraggingOverLeftHalf') ||
                  this.props.frameProps.get('tabIsDraggingOverRightHalf')
              })
            }
            ref='tab'
            draggable='true'
            title={this.titleValue}
            onClick={this.setActiveFrame.bind(this)}
            onDragStart={this.onDragStart.bind(this)}
            onDragEnd={this.onDragEnd.bind(this)}
            onDragLeave={this.onDragLeave.bind(this)}
            onDragOver={this.onDragOver.bind(this)}
            onDrop={this.onDrop.bind(this)}
        >
          <div className={loadingIcon} style={favIcon}></div>
          <div className='tabTitle'>{this.titleValue}</div>
          <div className='tabCloseBtn fa fa-times-circle'
            onClick={this.closeFrame.bind(this)}></div>
        </div>
        <Draggable active={this.props.frameProps.get('tabIsDraggingOverRightHalf')}/>
      </div>
    )
  }
}

export default Tab
