import React from 'react'

class ImmutableComponent extends React.Component {
  shouldComponentUpdate (nextProps) {
    return Object.keys(nextProps).some((prop) => nextProps[prop] !== this.props[prop])
  }
}

export default ImmutableComponent
