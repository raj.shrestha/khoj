import React from 'react'
import AppActions from '../actions/app.actions'
import ImmutableComponent from './immutable.component'
import UrlBar from './urlbar.component'

class NavigationBar extends ImmutableComponent{
  onReload () {
    process.emit('reload-active-frame')
  }

  onStop () {
    process.emit('stop-active-frame')
  }

  onForward () {
    process.emit('goForward-active-frame')
  }

  onBack () {
    process.emit('goBack-active-frame')
  }

  render() {
    const activeFrame = this.props.activeFrame
    const frameProps = this.props.activeFrame
    if(!frameProps){
      return null
    }
    return(
      <div className='navigationBar'>
        <div className='prevNextBtns'>
          <span
              className='prev fa fa-arrow-left'
              title='Click to go back'
              disabled={!activeFrame || !activeFrame.get('canGoBack')}
              onClick={this.onBack.bind(this)}>
          </span>
          <span
            className='next fa fa-arrow-right'
            title='Click to go forward'
            disabled={!activeFrame || !activeFrame.get('canGoForward')}
            onClick={this.onForward.bind(this)}>
          </span>
          {
            activeFrame.get('loading') ?
            <span
              className='stop fa fa-times'
              onClick={this.onStop.bind(this)}>
            </span> :
            <span
              className='reload fa fa-repeat'
              onClick={this.onReload.bind(this)}>
            </span>
          }
        </div>
        <UrlBar
          ref='urlBar'
          urlbar={this.props.navbar.get('urlbar')}
          sites={this.props.sites}
          activeFrameProps={frameProps}
          frames={this.props.frames}
          activeFrame={activeFrame}
          focused={this.props.focused}
          searchDetail={this.props.searchDetail}
          searchSuggestions={this.props.searchSuggestions}
        />
        <div className='tools'>
          <div className='menuBtn fa fa-bars'></div>
        </div>
      </div>
    )
  }
}

export default NavigationBar
