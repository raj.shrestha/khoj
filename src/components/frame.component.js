import React from 'react'
import ReactDOM from 'react-dom'
import {ipcRenderer, remote} from 'electron'
import classNames from 'class-name'
import UrlUtil from '../utils/url.util'
import AppActions from '../actions/app.actions'
import ImmutableComponent from './immutable.component'

class Frame extends ImmutableComponent{
  constructor () {
    super()
    //TODO: need to refactor the code
    ipcRenderer.on('shortcut-stop', () => {
      let webview = ReactDOM.findDOMNode(this.refs.webview)
      if (webview && this.props.isActive) {
        webview.stop()
      }
    })

    ipcRenderer.on('shortcut-reload', () => {
      let webview = ReactDOM.findDOMNode(this.refs.webview)
      if (webview && this.props.isActive) {
        webview.reload()
      }
    })

    ipcRenderer.on('shortcut-zoom-in', () => {
      let webview = ReactDOM.findDOMNode(this.refs.webview)
      if (webview && this.props.isActive) {
        webview.send('zoom-in')
      }
    })

    ipcRenderer.on('shortcut-zoom-out', () => {
      let webview = ReactDOM.findDOMNode(this.refs.webview)
      if (webview && this.props.isActive) {
        webview.send('zoom-out')
      }
    })

    ipcRenderer.on('shortcut-zoom-reset', () => {
      let webview = ReactDOM.findDOMNode(this.refs.webview)
      if (webview && this.props.isActive) {
        webview.send('zoom-reset')
      }
    })

    ipcRenderer.on('shortcut-toggle-developer-tools', () => {
      let webview = ReactDOM.findDOMNode(this.refs.webview)
      if (this.props.isActive){
        if (webview.isDevToolsOpened()) {
          webview.closeDevTools()
        } else {
          webview.openDevTools()
        }
      }
    })


    ipcRenderer.on('shortcut-view-source', () => {
      let webview = ReactDOM.findDOMNode(this.refs.webview)
      if(webview && this.props.isActive){
        let location = UrlUtil.getViewSourceUrlFromUrl(webview.getURL())
        AppActions.newFrame({
          location: location
        })
      }
    })

    ipcRenderer.on('shortcut-save-as', () => {
      let webview = ReactDOM.findDOMNode(this.refs.webview)
      if(webview && this.props.isActive){
        remote.getCurrentWebContents().downloadURL(webview.getURL())
      }
    })

    ipcRenderer.on('shortcut-print', () => {
      let webview = ReactDOM.findDOMNode(this.refs.webview)
      if(webview && this.props.isActive){
        webview.send('print-page')
      }
    })

    ipcRenderer.on('shortcut-open-finder', () => {
      if(this.props.isActive)
        AppActions.openFinder(this.props.frame)
    })

    ipcRenderer.on('shortcut-inspect-element', (e, props) => {
      let webview = ReactDOM.findDOMNode(this.refs.webview)
      if(webview && this.props.isActive){
        webview.inspectElement(props.x, props.y)
      }
    })

    process.on('reload-active-frame', () => {
      if (this.props.isActive) {
        let webview = ReactDOM.findDOMNode(this.refs.webview)
        if (webview && this.props.isActive) {
          webview.reload()
        }
      }
    })

    process.on('goForward-active-frame', () => {
      if (this.props.isActive) {
        let webview = ReactDOM.findDOMNode(this.refs.webview)
        if (webview && this.props.isActive) {
          webview.goForward()
        }
      }
    })

    process.on('goBack-active-frame', () => {
      if (this.props.isActive) {
        let webview = ReactDOM.findDOMNode(this.refs.webview)
        if (webview) {
          webview.goBack()
        }
      }
    })

    process.on('stop-active-frame', () => {
      if (this.props.isActive) {
        let webview = ReactDOM.findDOMNode(this.refs.webview)
        if (webview) {
          webview.stop()
        }
      }
    })
  }

  componentDidMount () {
    //TODO: need to refactor the code
    let webview = ReactDOM.findDOMNode(this.refs.webview)
    webview.addEventListener('new-window', (event) => {
      console.log('new window: ' + event.url)
      AppActions.newFrame({
          location: event.url
        },
        true
      )
    })
    webview.addEventListener('close', () => {
      console.log('close window')
    })
    webview.addEventListener('enter-html-full-screen', () => {
      console.log('enter html full screen')
      AppActions.enterHtmlFullScreen(this.props.frame)
    })
    webview.addEventListener('leave-html-full-screen', () => {
      console.log('leave html full screen')
      AppActions.exitHtmlFullScreen(this.props.frame)
    })
    webview.addEventListener('page-favicon-updated', ({favicons}) => {
      console.log('favicon updated')
      AppActions.setFavicons(this.props.frame, favicons)
    })
    webview.addEventListener('page-title-set', ({title}) => {
      console.log('title: ' + title)
      AppActions.setTitle(this.props.frame, title)
    })
    webview.addEventListener('dom-ready', () => {
      console.log('dom is ready')
    })
    webview.addEventListener('load-commit', (event) => {
      console.log('load commit')
      if (event.isMainFrame) {
        let key = this.props.frame.get('key')
        AppActions.setLocation(event.url, key)
      }
    })
    webview.addEventListener('did-get-redirect-request', (event) => {
      console.log('get redirect request')
    })
    webview.addEventListener('did-start-loading', (event) => {
      console.log('start loading')
      AppActions.webviewLoadingStart(
        this.props.frame)
    })
    webview.addEventListener('did-stop-loading', () => {
      console.log('stop loading')
      AppActions.webviewLoadingEnd(
        this.props.frame
      )
    })
    webview.addEventListener('did-fail-load', () => {
      console.log('did fail load')
    })
    webview.addEventListener('did-finish-load', () => {
      console.log('did finish load')
      console.log('Can go forward: '+this.refs.webview.canGoForward())
      AppActions.updateBackForwardState(
        this.props.frame,
        this.refs.webview.canGoBack(),
        this.refs.webview.canGoForward()
      )
    })
  }

  onCloseFinder () {
    let webview = ReactDOM.findDOMNode(this.refs.webview)
    webview.stopFindInPage('clearSelection')
    AppActions.closeFinder(this.props.frame)
  }

  onKeyDown (e) {
    switch (e.keyCode) {
      case 13:
        e.preventDefault()
        let webview = ReactDOM.findDOMNode(this.refs.webview)
        if(e.target.value)
          webview.findInPage(e.target.value, {forward: false, findNext: false})
        break

      default:

    }
  }

  onFindInPageNext (e) {
    let webview = ReactDOM.findDOMNode(this.refs.webview)
    let finderInput = ReactDOM.findDOMNode(this.refs.finderInput)
    if(finderInput.value)
      webview.findInPage(finderInput.value, {forward: true})
  }

  onFindInPagePrevious (e) {
    let webview = ReactDOM.findDOMNode(this.refs.webview)
    let finderInput = ReactDOM.findDOMNode(this.refs.finderInput)
    if(finderInput.value)
      webview.findInPage(finderInput.value, {forward: false})
  }

  render () {
    return (
      <div className={classNames({
          frameWrapper: true,
          active: this.props.isActive
        })}>
        <div className={classNames({
            findbar: true,
            active: this.props.frame.get('finder')
          })} >
          <div className='closeFinder'>
            <span className='btn fa fa-times'
              onClick={this.onCloseFinder.bind(this)} >
            </span>
          </div>
            <form id='finder'>
              <input
                ref='finderInput'
                id='finderInput'
                onKeyDown={this.onKeyDown.bind(this)}
              />
            </form>
          <div className='nextFinder'>
            <span className='btn fa fa-chevron-up'
              onClick={this.onFindInPageNext.bind(this)} >
            </span>
          </div>
          <div className='prevFinder'>
            <span className='btn fa fa-chevron-down'
              onClick={this.onFindInPagePrevious.bind(this)} >
            </span>
          </div>
        </div>
        <div className='webContainer' >
          <webview
            ref='webview'
            src={this.props.frame.get('src')}
            preload='preload.js'
          />
        </div>
      </div>
    )
  }
}

export default Frame
