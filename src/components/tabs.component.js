import React from 'react'
import AppActions from '../actions/app.actions'
import ImmutableComponent from './immutable.component'
import Tab from './tab.component'

class Tabs extends ImmutableComponent{
  constructor () {
    super()
  }

  onNewFrame () {
    AppActions.newFrame()
    process.emit('active-urlbar')
  }

  render () {
    return(
      <div className='tabBar platform--darwin'>
        <div className='tabs'>
          <div className='tabContainer'>
            {
              this.props.frames.map(frameProps => <Tab
                frameProps={frameProps}
                key={'tab-' + frameProps.get('key')}
                isActive={this.props.activeFrame === frameProps}
                activeDraggedTab={this.props.tabs.get('activeDraggedTab')}
              />)
            }
            <div className='newTabBtn'>
              <span className='fa fa-plus' onClick={this.onNewFrame.bind(this)}></span>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default Tabs