import {ipcRenderer} from 'electron'
import AppConstants from '../constants/app.constants'
import AppDispatcher from '../dispatcher/app.dispatcher'
import AppStore from '../stores/app.stores'
import UrlUtil from '../utils/url.util'

const AppActions = {
  loadUrl: function (location) {
    if (UrlUtil.isURL(location)) {
      location = UrlUtil.getUrlFromInput(location)
    } else {
      location = 'https://www.google.com.np/search?q=' + location.split(' ').join('+')
    }

    AppDispatcher.dispatch({
      actionType: AppConstants.SET_URL,
      location
    })
  },

  setUrlBarInput: function (location) {
    AppDispatcher.dispatch({
      actionType: AppConstants.SET_URLBAR_INPUT,
      location
    })
  },

  newFrame: function (frameProps = {}, openInBackground) {
    frameProps.location = frameProps.location
    AppDispatcher.dispatch({
      actionType: AppConstants.NEW_FRAME,
      frameProps: frameProps,
      openInBackground: openInBackground || false
    })
  },

  setTitle: function (frameProps, title) {
    AppDispatcher.dispatch({
      actionType: AppConstants.SET_TITLE,
      frameProps,
      title
    })
  },

  webviewLoadingStart: function (frameProps) {
    AppDispatcher.dispatch({
      actionType: AppConstants.WEBVIEW_LOADING_START,
      frameProps
    })
  },

  webviewLoadingEnd: function (frameProps) {
    AppDispatcher.dispatch({
      actionType: AppConstants.WEBVIEW_LOADING_END,
      frameProps
    })
  },

  setActiveFrame: function (frameProps) {
    AppDispatcher.dispatch({
      actionType: AppConstants.SET_ACTIVE_FRAME,
      frameProps: frameProps
    })
  },

  closeFrame: function (frameProps) {
    if (AppStore.getFrameCount() > 1) {
      AppDispatcher.dispatch({
        actionType: AppConstants.CLOSE_FRAME,
        frameProps
      })
    } else {
      this.closeWindow()
    }
  },

  undoClosedFrame: function () {
    AppDispatcher.dispatch({
      actionType: AppConstants.UNDO_CLOSED_FRAME
    })
  },

  setUrlBarActive: function (isActive) {
    AppDispatcher.dispatch({
      actionType: AppConstants.SET_URL_BAR_ACTIVE,
      isActive
    })
  },

  setUrlBarFocused: function (focused) {
    AppDispatcher.dispatch({
      actionType: AppConstants.SET_URLBAR_FOCUSED,
      focused
    })
  },

  setLocation: function (location, key) {
    if (UrlUtil.isURL(location)) {
      location = UrlUtil.getUrlFromInput(location)
    }
    AppDispatcher.dispatch({
      actionType: AppConstants.SET_LOCATION,
      location: location,
      key: key
    })
  },

  updateBackForwardState: function (frameProps, canGoBack, canGoForward) {
    AppDispatcher.dispatch({
      actionType: AppConstants.UPDATE_BACKWARD_FORWARD,
      frameProps,
      canGoBack,
      canGoForward
    })
  },

  tabDragStart: function (frameProps) {
    AppDispatcher.dispatch({
      actionType: AppConstants.TAB_DRAG_START,
      frameProps
    })
  },

  tabDragStop: function (frameProps) {
    AppDispatcher.dispatch({
      actionType: AppConstants.TAB_DRAG_STOP,
      frameProps
    })
  },

  tabDraggingOn: function (frameProps) {
    AppDispatcher.dispatch({
      actionType: AppConstants.TAB_DRAGGING_ON,
      frameProps
    })
  },

  tabDragDraggingOverLeftHalf: function (frameProps) {
    AppDispatcher.dispatch({
      actionType: AppConstants.TAB_DRAGGING_OVER_LEFT,
      frameProps
    })
  },

  tabDragDraggingOverRightHalf: function (frameProps) {
    AppDispatcher.dispatch({
      actionType: AppConstants.TAB_DRAGGING_OVER_RIGHT,
      frameProps
    })
  },

  tabDragExit: function (frameProps) {
    AppDispatcher.dispatch({
      actionType: AppConstants.TAB_DRAG_EXIT,
      frameProps
    })
  },

  tabDragExitRightHalf: function (frameProps) {
    AppDispatcher.dispatch({
      actionType: AppConstants.TAB_DRAG_EXIT_RIGHT,
      frameProps
    })
  },

  tabMove: function (sourceFrameProps, destinationFrameProps, prepend) {
    AppDispatcher.dispatch({
      actionType: AppConstants.TAB_MOVE,
      sourceFrameProps,
      destinationFrameProps,
      prepend
    })
  },

  closeWindow: function () {
    ipcRenderer.send('close-window')
  },

  setUrlBarSuggestions: function (suggestionList, selectedIndex) {
    AppDispatcher.dispatch({
      actionType: AppConstants.SET_URL_BAR_SUGGESTIONS,
      suggestionList,
      selectedIndex
    })
  },

  setUrlBarPreview: function (value) {
    AppDispatcher.dispatch({
      actionType: AppConstants.SET_URL_BAR_PREVIEW,
      value
    })
  },

  setUrlBarSuggestionSearchResults: function (searchResults) {
    AppDispatcher.dispatch({
      actionType: AppConstants.SET_URL_BAR_SUGGESTION_SEARCH_RESULTS,
      searchResults
    })
  },

  enterHtmlFullScreen: function (frameProps) {
    AppDispatcher.dispatch({
      actionType: AppConstants.TOGGLE_FULL_HTML_SCREEN,
      frameProps,
      value: true
    })
  },

  exitHtmlFullScreen: function (frameProps) {
    AppDispatcher.dispatch({
      actionType: AppConstants.TOGGLE_FULL_HTML_SCREEN,
      frameProps,
      value: false
    })
  },

  setFavicons: function (frameProps, favicons) {
    AppDispatcher.dispatch({
      actionType: AppConstants.SET_FRAME_FAVICONS,
      frameProps,
      favicons
    })
  },

  openFinder: function (frameProps) {
    AppDispatcher.dispatch({
      actionType: AppConstants.OPEN_FINDER_BAR,
      frameProps,
      open: true
    })
  },

  closeFinder: function (frameProps) {
    AppDispatcher.dispatch({
      actionType: AppConstants.OPEN_FINDER_BAR,
      frameProps,
      open: false
    })
  }
}

export default AppActions
