import { Dispatcher } from 'flux';

class AppDispatcher extends Dispatcher {
  /**
   *  A bridge function between the views and the dispatcher, marking the action
   * as a view action.
   * @param  {object} action data from the view.
   */
  handleViewAction (action) {
    this.dispatch({
      source: 'VIEW_ACTION',
      action: action
    })
  }
}

const appDispatcher = new AppDispatcher()

export default appDispatcher
