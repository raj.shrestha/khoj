const electron = require('electron')
const Menu = electron.Menu || electron.remote.Menu

const onContextMenu = (nodeProps) => {
  let template = [
    {
      label: 'Back',
      enable: false
    },
    {
      label: 'Forward',
      enable: false
    },
    {
      label: 'Reload',
      click: (item, focusedWindow) => {
        if (focusedWindow) {
          focusedWindow.webContents.send('shortcut-reload')
        }
      }
    },
    {
      type: 'separator'
    },
    {
      label: 'Save As',
      click: (item, focusedWindow) => {
        if (focusedWindow) {
          focusedWindow.webContents.send('shortcut-save-as')
        }
      }
    },
    {
      label: 'Print',
      click: function (item, focusedWindow) {
        if (focusedWindow) {
          focusedWindow.webContents.send('shortcut-print')
        }
      }
    },
    {
      type: 'separator'
    },
    {
      label: 'View Page Source',
      click: (item, focusedWindow) => {
        if (focusedWindow) {
          focusedWindow.webContents.send('shortcut-view-source')
        }
      }
    },
    {
      label: 'Inspect Element',
      click: (item, focusedWindow) => {
        if (focusedWindow && nodeProps) {
          focusedWindow.webContents.send('shortcut-inspect-element', nodeProps)
        }
      }
    }
  ]
  let nodeName = nodeProps.name
  switch (nodeName) {
    case 'A':
      template.push({
        label: 'Open in new tab',
        click: (item, focusedWindow) => {
          if (focusedWindow && nodeProps.src) {
            console.log('Open in new tab')
            focusedWindow.webContents.send('shortcut-new-frame', nodeProps.src, true)
          }
        }
      })
      template.push({
        label: 'Open in new private tab',
        enabled: false
      })
      break
    case 'IMG':
      template.push({
        label: 'Save image...',
        click: (item, focusedWindow) => {
          if (focusedWindow && nodeProps.src) {
            focusedWindow.webContents.downloadURL(nodeProps.src)
          }
        }
      })
      template.push({
        label: 'Open image in new tab',
        click: (item, focusedWindow) => {
          if (focusedWindow && nodeProps.src) {
            // TODO: open this in the next tab instead of last tab
            focusedWindow.webContents.send('shortcut-new-frame', nodeProps.src, true)
          }
        }
      })
      break
  }

  const menu = Menu.buildFromTemplate(template)
  menu.popup(electron.remote.getCurrentWindow())
}

module.exports.onContextMenu = onContextMenu
