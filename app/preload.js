var webFrame = require('electron').webFrame
var ipcRenderer = require('electron').ipcRenderer
var ipcMain = require('electron').ipcMain
var Menu = require('electron').Menu;
var MenuItem = require('electron').MenuItem;

var browserZoomLevel = 0
var browserMaxZoom = 9
var browserMinZoom = -8

ipcRenderer.on('zoom-in', function () {
  if (browserMaxZoom > browserZoomLevel) {
    browserZoomLevel += 1
  }
  webFrame.setZoomLevel(browserZoomLevel)
})

ipcRenderer.on('zoom-out', function () {
  if (browserMinZoom < browserZoomLevel) {
    browserZoomLevel -= 1
  }
  webFrame.setZoomLevel(browserZoomLevel)
})

ipcRenderer.on('zoom-reset', function () {
  browserZoomLevel = 0
  webFrame.setZoomLevel(browserZoomLevel)
})

ipcRenderer.on('print-page', function () {
  window.print()
})

document.addEventListener('contextmenu', (e) => {
  var name = e.target.nodeName.toUpperCase()
  var nodeProps = {
    name: name,
    src: name === 'A' ? e.target.href : e.target.src,
    x: e.x,
    y: e.y
  }

  ipcRenderer.send('context-menu-opened', nodeProps)
  e.preventDefault()
}, false)
