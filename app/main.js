'use strict'

const electron = require('electron')
const app = electron.app
const BrowserWindow = electron.BrowserWindow
const ipcMain = electron.ipcMain
const electronLocalshortcut =  require('electron-localshortcut')
const Menu = require('./menu')

let windows = []

const createWindow = () => {
  let screen = electron.screen
  let primaryDisplay = screen.getPrimaryDisplay()
  let mainWindow = new BrowserWindow({
    minHeight: 500,
    minWidth: 300,
    width:  Math.floor(primaryDisplay.bounds.width),
    height: Math.floor(primaryDisplay.bounds.height),
    resizable: true,
    titleBarStyle: 'hidden-inset'
  })

  // and load the index.html of the app.
  mainWindow.loadURL(`file://${__dirname}/index.html`)

  // Open the DevTools.
  if(process.env.ENVIRONMENT === 'DEV')
    mainWindow.webContents.openDevTools()

  mainWindow.on('closed', function () {
    var index = windows.indexOf(mainWindow)
    if (index > -1) {
      windows.splice(index, 1)
    }
  })

  return mainWindow
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', function() {
  windows.push(createWindow())

  ipcMain.on('quit-application', () => {
    app.quit()
  })

  ipcMain.on('new-window', () => windows.push(createWindow()))
  process.on('new-window', () => windows.push(createWindow()))

  ipcMain.on('close-window', () => BrowserWindow.getFocusedWindow().close())
  process.on('close-window', () => BrowserWindow.getFocusedWindow().close())

  ipcMain.on('context-menu-opened', (e, nodeName) => {
    BrowserWindow.getFocusedWindow().webContents.send('context-menu-opened', nodeName)
  })
  // Most of these events will simply be listened to by the app store and acted
  // upon.  However sometimes there are no state changes, for example with focusing
  // the URL bar.  In those cases it's acceptable for the individual components to
  // listen to the events.
  const simpleWebContentEvents = [
    ['CmdOrCtrl+L', 'shortcut-focus-url'],
    ['Escape', 'shortcut-stop'],
    ['Ctrl+Tab', 'shortcut-next-tab'],
    ['Ctrl+Shift+Tab', 'shortcut-prev-tab'],
    ['CmdOrCtrl+R', 'shortcut-reload'],
    ['CmdOrCtrl+=', 'shortcut-zoom-in'],
    ['CmdOrCtrl+-', 'shortcut-zoom-out'],
    ['CmdOrCtrl+0', 'shortcut-zoom-reset'],
    ['CmdOrCtrl+Alt+I', 'shortcut-toggle-developer-tools'],
    ['CmdOrCtrl+Shift+T', 'shortcut-undo-closed-frame'],
    ['CmdOrCtrl+F', 'shortcut-open-finder']
  ]

  simpleWebContentEvents.forEach((shortcutEventName) =>
    electronLocalshortcut.register(shortcutEventName[0], () => {
      if(BrowserWindow.getFocusedWindow() && BrowserWindow.getFocusedWindow().webContents){
        BrowserWindow.getFocusedWindow().webContents.send(shortcutEventName[1])
      }
    })
  )

  electronLocalshortcut.register('CmdOrCtrl+Shift+J', () => {
    BrowserWindow.getFocusedWindow().toggleDevTools()
  })

  Menu.init()
})

// Quit when all windows are closed.
app.on('window-all-closed', () => {
  // On macOS it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', () => {
  // On macOS it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (windows.length === 0) {
    windows.push(createWindow())
  }
})
