const electron = require('electron')
const app = electron.app
const Menu = electron.Menu
const Shell = electron.Shell

const init = () => {
  var template = [
    {
      label: 'File',
      submenu: [
        {
          label: 'New Tab',
          accelerator: 'CmdOrCtrl+T',
          click: function (item, focusedWindow) {
            if(focusedWindow)
              focusedWindow.webContents.send('shortcut-new-frame')
          }
        }, {
          label: 'New Window',
          accelerator: 'CmdOrCtrl+N',
          click: () => process.emit('new-window')
        },
        {
          label: 'Reopen Closed Tab',
          accelerator: 'CmdOrCtrl+Shift+T',
          click: function (item, focusedWindow) {
            if(focusedWindow)
              focusedWindow.webContents.send('shortcut-undo-closed-frame')
          }
        },
        {
          type: 'separator'
        }, {
          label: 'Close Window',
          accelerator: 'CmdOrCtrl+Shift+W',
          click: () => process.emit('close-window')
        }, {
          label: 'Close Tab',
          accelerator: 'CmdOrCtrl+W',
          click: function (item, focusedWindow) {
            if(focusedWindow)
              focusedWindow.webContents.send('shortcut-close-frame')
          }
        },
        {
          label: 'Save As',
          accelerator: 'CmdOrCtrl+S',
          click: function (item, focusedWindow) {
            if(focusedWindow)
              focusedWindow.webContents.send('shortcut-save-as')
          }
        },
        {
          label: 'Print',
          accelerator:  'CmdOrCtrl+P',
          click: function (item, focusedWindow) {
            if(focusedWindow)
              focusedWindow.webContents.send('shortcut-print')
          }
        }
      ]
    }, {
      label: 'Edit',
      submenu: [
        {
          label: 'Undo',
          accelerator: 'CmdOrCtrl+Z',
          role: 'undo'
        }, {
          label: 'Redo',
          accelerator: 'Shift+CmdOrCtrl+Z',
          role: 'redo'
        }, {
          type: 'separator'
        }, {
          label: 'Cut',
          accelerator: 'CmdOrCtrl+X',
          role: 'cut'
        }, {
          label: 'Copy',
          accelerator: 'CmdOrCtrl+C',
          role: 'copy'
        }, {
          label: 'Paste',
          accelerator: 'CmdOrCtrl+V',
          role: 'paste'
        }, {
          label: 'Select All',
          accelerator: 'CmdOrCtrl+A',
          role: 'selectall'
        }, {
          type: 'separator'
        }, {
          label: 'Find',
          accelerator: 'CmdOrCtrl+F',
          click: function (item, focusedWindow) {
            if (focusedWindow)
              focusedWindow.webContents.send('shortcut-open-finder')
          }
        }
      ]
    }, {
      label: 'View',
      submenu: [
        {
          label: 'Stop',
          accelerator: 'CmdOrCtrl+.',
          click: function (item, focusedWindow) {
            if (focusedWindow)
              focusedWindow.webContents.send('shortcut-stop')
          }
        },
        {
          label: 'Reload',
          accelerator: 'CmdOrCtrl+R',
          click: function (item, focusedWindow) {
            if (focusedWindow) {
              focusedWindow.webContents.send.apply(focusedWindow.webContents, 'shortcut-reload')
              return true
            } else{
              return false
            }
          }
        },
       {
          label: 'Toggle Developer Tools',
          accelerator: (function () {
            if (process.platform === 'darwin') {
              return 'Alt+Command+I'
            } else {
              return 'Ctrl+Shift+I'
            }
          })(),
          click: function (item, focusedWindow) {
            if (focusedWindow) {
              focusedWindow.webContents.send('shortcut-toggle-developer-tools')
            }
          }
        },
        {
          type: 'separator'
        },
        {
          label: 'Actual Size',
          accelerator: (function () {
            if (process.platform === 'darwin') {
              return 'Command+0'
            } else {
              return 'Ctrl+0'
            }
          })(),
          click: function (item, focusedWindow) {
             if (focusedWindow) {
              focusedWindow.webContents.send('shortcut-zoom-reset')
            }
          }
        },
        {
          label: 'Zoom In',
          accelerator: (function () {
            if (process.platform === 'darwin') {
              return 'Command+='
            } else {
              return 'Ctrl+='
            }
          })(),
          click: function (item, focusedWindow) {
             if (focusedWindow) {
              focusedWindow.webContents.send('shortcut-zoom-in')
            }
          }
        },
       {
          label: 'Zoom Out',
          accelerator: (function () {
            if (process.platform === 'darwin') {
              return 'Command+-'
            } else {
              return 'Ctrl+-'
            }
          })(),
          click: function (item, focusedWindow) {
             if (focusedWindow) {
              focusedWindow.webContents.send('shortcut-zoom-out')
            }
          }
        },
        {
          type: 'separator'
        },
        {
          label: 'Toggle Full Screen',
          accelerator: (function () {
            if (process.platform === 'darwin') {
              return 'Ctrl+Command+F'
            } else {
              return 'F11'
            }
          })(),
          click: function (item, focusedWindow) {
            if (focusedWindow) {
              focusedWindow.setFullScreen(!focusedWindow.isFullScreen())
            }
          }
        },
      ]
    }, {
      label: 'Window',
      role: 'window',
      submenu: [
        {
          label: 'Minimize',
          accelerator: 'CmdOrCtrl+M',
          role: 'minimize'
        }
      ]
    }, {
      label: 'Help',
      role: 'help',
      submenu: [
        {
          label: 'Learn More',
          enable: false
        }
      ]
    }
  ]

  if (process.platform === 'darwin') {
    var name = 'Khoj'
    template.unshift({
      label: name,
      submenu: [
        {
          label: 'About ' + name,
          role: 'about'
        }, {
          type: 'separator'
        }, {
          label: 'Services',
          role: 'services',
          submenu: []
        }, {
          type: 'separator'
        }, {
          label: 'Hide ' + name,
          accelerator: 'Command+H',
          role: 'hide'
        }, {
          label: 'Hide Others',
          accelerator: 'Command+Shift+H',
          role: 'hideothers'
        }, {
          label: 'Show All',
          role: 'unhide'
        }, {
          type: 'separator'
        }, {
          label: 'Quit',
          accelerator: 'Command+Q',
          click: app.quit
        }
      ]
    })
    // Window menu.
    template[3].submenu.push(
      {
        type: 'separator'
      }, {
        label: 'Bring All to Front',
        role: 'front'
      }
    )
  }

  const menu = Menu.buildFromTemplate(template)
  Menu.setApplicationMenu(menu)
}

module.exports.init = init
