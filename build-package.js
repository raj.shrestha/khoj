// var gulp = require('gulp')
const path = require('path')
var exec = require('child_process').exec;

const isWindows = process.platform === 'win32'
const isDarwin = process.platform === 'darwin'
var arch = 'x64'
const isLinux = process.platform === 'linux'

var appIcon
if (isWindows) {
  appIcon = 'res/app.ico'
  if (process.env.TARGET_ARCH === 'ia32') {
    arch = 'ia32'
  }
} else if (isDarwin) {
  appIcon = 'res/app.icns'
} else {
  appIcon = 'res/app.png'
}

var buildDir = 'Khoj-' + process.platform + '-' + arch

var cmds = ['echo start cleaning ...']

cmds = cmds.concat(['rm -Rf ' + buildDir])

cmds = cmds.concat([
  'echo done',
  'echo start buildling...'
])

cmds = cmds.concat([
  '"./node_modules/.bin/webpack"',
  './node_modules/.bin/electron-packager . Khoj --overwrite --ignore=\"electron-packager|electron-prebuilt|babel-*\" --platform=darwin --arch=x64 --version=1.3.4 --icon=res/app.icns',
  './node_modules/.bin/asar pack Khoj-darwin-x64/Khoj.app/Contents/Resources/app Khoj-darwin-x64/Khoj.app/Contents/Resources/app.asar',
  'rm -rf Khoj-darwin-x64/Khoj.app/Contents/Resources/app'
])


var cmd = ''
cmd += cmds.join('&&')
console.log(cmd)
var cb = console.log.bind(null, 'Packaging done!!!')
var r = exec(cmd, {
  env: process.env
}, function () {
  if (cb) {
    cb()
  }
})
r.stdout.pipe(process.stdout)
r.stderr.pipe(process.stderr)


